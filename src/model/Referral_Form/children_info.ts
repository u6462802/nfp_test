export interface Children_info {
  first_name: string;
  middle_name: string;
  last_name: string;
  birthday: string;
  Gender: any;
  c_id: any;
  currently_residing: any;
}
