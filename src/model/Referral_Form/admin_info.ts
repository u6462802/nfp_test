export interface Admin_info {
  date: any;
  call_len: any;
  referred_toOneLink: boolean;
  completed_by: string;
  referred_form:string;
  contact_info:string;
}
