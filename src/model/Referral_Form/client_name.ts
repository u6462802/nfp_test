export interface Client_name {
  name_known: boolean;
  first_name: string;
  middle_name: string;
  last_name: string;
}
