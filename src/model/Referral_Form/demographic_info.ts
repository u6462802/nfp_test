export interface Demographic_info {
  birthday: any;
  age: number;
  atsi: any;
  atsi_id: string;
  cultural_id:string;
  country_birth:string;
  arrival_date: any;
  home_language: string;
  interpreter: boolean;
  single_married: any;
}
