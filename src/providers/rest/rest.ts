import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders} from "@angular/common/http";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  apiUrl = 'http://127.0.0.1:5000';
  constructor(public http: HttpClient) {

  }

  postForm(path,data){
    let headers = new HttpHeaders().set('Content-Type','application/json');
    let body = JSON.stringify(data);
    // console.log(JSON.stringify(form.value));
    let res = this.http.post(this.apiUrl+path,body,{headers:headers})
      .subscribe(data => {
        console.log(data);
        if(data =='success'){
          alert('Success');
        }else {
          alert('Fail to submit');
        }
      });
  }

}
