webpackJsonp([0],{

/***/ 111:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 111;

/***/ }),

/***/ 153:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 153;

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.splash = true;
    }
    LoginPage.prototype.doLogin = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
                }
                catch (e) {
                    console.log(e);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.passwordReset = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                try {
                    console.log('password reset');
                }
                catch (e) {
                    console.error(e);
                }
                return [2 /*return*/];
            });
        });
    };
    LoginPage.prototype.register = function () {
        console.log('register');
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () { return _this.splash = false; }, 4000);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/shi/WebstormProject/test/src/pages/login/login.html"*/'<div id="custom-overlay" [style.display]="splash ? \'flex\': \'none\'">\n  <div class="flb">\n    <div class="Aligner-item Aligner-item--top"></div>\n    <img src="assets/ionic_logo.svg">\n    <div class="Aligner-item Aligner-item--bottom"></div>\n  </div>\n</div>\n\n\n<ion-content class="bg">\n  <br>\n  <ion-row class="row">\n    <ion-col>\n      <img src="assets/imgs/lightapp.png"/>\n    </ion-col>\n  </ion-row>\n  <br><br>\n\n<ion-grid fixed="md">\n  <ion-item class="enter" style="border-radius: 10px">\n    <ion-label><ion-icon name="contact" ></ion-icon></ion-label>\n    <ion-input type="text" style="color: #002a3e"  ></ion-input>\n  </ion-item>\n  <ion-item class="enter" style="border-radius: 10px" no-lines>\n    <ion-label><ion-icon name="lock" ></ion-icon></ion-label>\n    <ion-input type="password" style="color: #002a3e" ></ion-input>\n  </ion-item>\n</ion-grid>\n  <br><br><br><br>\n  <ion-grid fixed="md">\n   <button ion-button block outline (click)="doLogin(user)"><b>Log-in</b></button>\n   <button ion-button block outline (click)="register()"><b>Register</b></button>\n   <button ion-button clear block  (click)="passwordReset(user)" icon-left style="font-size: 13px" >\n     <ion-icon name="help-circle"></ion-icon>\n        Forget Password\n     </button>\n  </ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProject/test/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_constant_list_country__ = __webpack_require__(276);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, formBulider, rest) {
        this.navCtrl = navCtrl;
        this.formBulider = formBulider;
        this.rest = rest;
        //data model
        this.refered_before = {};
        this.client_name = {};
        this.client_address = {};
        this.d_info = {};
        this.service_re = {};
        this.other_info = {};
        this.follow_up = {};
        this.admin_info = {};
        this.pre_status = {};
        this.presenting_summary = {};
        this.children_info = {};
        this.childrenArray = [];
        this.children_number = [];
        this.number = 0;
        this.checkedIdx = 1;
        this.section_2_index = 1;
        this.section_atsi = 1;
        this.section_Interpreter = 1;
        this.section_Unmet = 1;
        this.section_Naha = 1;
        this.section_MeetCriteri = 1;
        this.section_sm = 1;
        this.follow_up_required = 1;
        this.referred_link = 1;
        this.section_gender = [];
        this.service_name = [
            'Housing at Karyina',
            'Outreach',
            'Telephone Counselling',
            'Material Assistance',
            'Information and Retrieval',
            'Other'
        ];
        this.options = [
            'Yes',
            'No',
            'Unknown'
        ];
        this.section_2 = [
            'Yes',
            'No'
        ];
        this.single_married = [
            'Single',
            'Married'
        ];
        this.gender = [
            'Male',
            'Female'
        ];
        this.country_list = __WEBPACK_IMPORTED_MODULE_4__model_constant_list_country__["a" /* Country */].country_list;
        var today = new Date();
        var dd = today.getMonth() + 1;
        var day;
        if (dd <= 9) {
            day = '0' + dd;
        }
        this.today = today.getFullYear() + '-' + day + '-' + today.getDate();
        console.log(this.today);
        // this.myGroup = formBulider.group({
        //   type_housing:['',Validators.required],
        //   // birthday:['',Validators.required],
        //   // age:['',Validators.required]
        //   // atsi_id:['',Validators.required]
        //
        //
        // });
        //
        // this.client_address.type_housing = this.myGroup.controls['type_housing'];
        // // this.d_info.birthday = this.myGroup.controls['birthday'];
        // // this.d_info.age = this.myGroup.controls['age'];
        // // this.section_atsi = this.myGroup.controls['atsi_id']
        this.d_info.age = 0;
    }
    HomePage.prototype.addChildren = function () {
        this.section_gender.push(1);
        this.childrenArray.push({});
        console.log(this.childrenArray);
        console.log(this.section_gender);
    };
    HomePage.prototype.removeChildren = function () {
        this.section_gender.pop();
        this.childrenArray.pop();
    };
    HomePage.prototype.days_left = function () {
        var oneDay = 24 * 60 * 60 * 1000;
        var dateTime = new Date();
        var edb = new Date(this.pre_status.edb);
        var diffDays = Math.round(Math.abs((edb.getTime() - dateTime.getTime()) / (oneDay)));
        this.edb_left = diffDays;
    };
    HomePage.prototype.convent = function (choose) {
        if (choose == "Yes") {
            return true;
        }
        else {
            return false;
        }
    };
    HomePage.prototype.calculate_age = function () {
        var dateTime = new Date();
        var age = dateTime.getFullYear();
        this.d_info.age = age - parseInt(this.d_info.birthday);
    };
    HomePage.prototype.sendForm = function (form) {
        this.client_name.name_known = this.convent(this.section_2[this.section_2_index]);
        this.refered_before.referred = this.options[this.checkedIdx];
        this.d_info.atsi = this.convent(this.section_2[this.section_atsi]);
        this.d_info.interpreter = this.convent(this.section_2[this.section_Interpreter]);
        this.d_info.single_married = this.single_married[this.section_sm];
        for (var i = 0; i < this.childrenArray.length; i++) {
            this.childrenArray[i].Gender = this.gender[this.section_gender[i]];
        }
        console.log(this.childrenArray);
        var data = {
            "referred_before": this.refered_before,
            "client_name": this.client_name,
            "address": this.client_address,
            "demographic Information": this.d_info,
            "service_required": this.service_re,
            "other_info": this.other_info,
            "pregnancy_status": this.pre_status,
            "presenting_situation": this.presenting_summary,
            "follow_up": this.follow_up_required,
            "admin_info": this.admin_info,
            "children_info": this.childrenArray
        };
        console.log(JSON.stringify(data));
        var res = this.rest.postForm('/json-example', data);
    };
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/shi/WebstormProject/test/src/pages/home/home.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Client Referral Form\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid fixed="md">\n  <form #myForm="ngForm" (ngSubmit)="sendForm(myForm)">\n    <ion-item-divider><h2 text-wrap> Has the client been referred to Karinya House before?</h2></ion-item-divider>\n  <ion-list>\n  <ion-item>\n    <ion-grid item-content>\n      <ion-row>\n        <ion-col *ngFor="let item of options; let i=index">\n          <ion-label>{{item}}</ion-label>\n          <ion-checkbox item-right [ngModel]="checkedIdx == i"(ngModelChange)="$event ? checkedIdx = i : checkedIdx = -1" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item>\n  </ion-list>\n\n    <ion-item-divider><br> <h2 text-wrap>Name of Client</h2></ion-item-divider>\n\n  <ion-list>\n      <ion-item no-lines>\n        <ion-grid item-content>\n          <br>\n          <ion-row><header>Is the client name known?</header></ion-row>\n          <ion-row>\n            <ion-col *ngFor="let item of section_2; let i=index">\n              <ion-label>{{item}}</ion-label>\n              <ion-checkbox item-right [ngModel] = "section_2_index == i" (ngModelChange)="$event ? section_2_index = i : section_2_index = -1" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n    <ion-item-group *ngIf="section_2_index == 1">\n      <ion-grid>\n        <ion-row>\n        <ion-col col-auto>\n          <ion-item>\n            <ion-label class="fixedLabel">First Name:</ion-label>\n            <ion-input type="text" [(ngModel)]="client_name.first_name" style="border: solid;border-width: 1px" name="firstname"></ion-input>\n          </ion-item>\n        </ion-col>\n      <br>\n        <ion-col col-lg-auto>\n          <ion-item no-lines>\n            <ion-label class="fixedLabel">Middle Name:</ion-label>\n            <ion-input type="text" [(ngModel)]="client_name.middle_name" style="border: solid;border-width: 1px;" name="middlename" ></ion-input>\n          </ion-item>\n        </ion-col>\n      <br>\n        <ion-col col-auto>\n          <ion-item no-lines>\n            <ion-label class="fixedLabel">Last Name: </ion-label>\n            <ion-input type="text" [(ngModel)]="client_name.last_name" style="border: solid;border-width: 1px" name = "lastname"></ion-input>\n          </ion-item>\n        </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item-group>\n    <br>\n  </ion-list>\n\n    <ion-item-divider><br><h2 text-wrap> Address </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-item-group>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-auto>\n            <ion-item no-lines>\n              <ion-label class="fixedLabel">Address: </ion-label>\n              <ion-input type="text" [(ngModel)]="client_address.address" style="border: solid;border-width: 1px" name="address"></ion-input>\n            </ion-item>\n          </ion-col>\n      <br>\n          <ion-col col-auto>\n            <ion-item no-lines>\n              <ion-label class="fixedLabel">Postcode:</ion-label>\n              <ion-input type="text" [(ngModel)]="client_address.postcode"style="border: solid;border-width: 1px;" name="postcode"></ion-input>\n            </ion-item>\n          </ion-col>\n      <br>\n          <ion-col col-auto>\n            <ion-item>\n              <ion-label> Type of housing <span style="color: red">*</span></ion-label>\n              <ion-select [ngModel]="client_address.type_housing" required="required" name="housing_type" required #type="ngModel">\n                <ion-option value="test1">Type 1</ion-option>\n                <ion-option value="test2">Type 2</ion-option>\n                <ion-option value="test3">Type 3</ion-option>\n              </ion-select>\n            </ion-item>\n            <span *ngIf="type.errors && type.touched" text-end>\n                      <span [hidden]="!type.errors.required">\n                          <ion-label style="color: red; margin-left: 16px;"> This field is required </ion-label>\n                      </span>\n            </span>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item-group>\n  </ion-list>\n\n    <ion-item-divider><br><h2 text-wrap> Children information </h2></ion-item-divider>\n\n    <ion-list >\n      <br>\n      <span\n        ion-button\n        float-left\n        icon-left\n        clear\n        (click)="addChildren()" type="button">\n               <ion-icon name="add"></ion-icon>\n               Add\n         </span>\n      <span ion-button float-right icon-left clear (click)="removeChildren()" type="button"><ion-icon name="close"></ion-icon>Remove</span>\n      <ion-item-group *ngFor="let child of childrenArray; let i = index">\n        <ion-item-divider>\n          <br><h2> Child {{i+1}} </h2>\n        </ion-item-divider>\n        <br>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-auto>\n              <ion-item no-lines>\n                <ion-label class="fixedLabel">First Name: </ion-label>\n                <ion-input type="text" [(ngModel)]="childrenArray[i].first_name" style="border: solid;border-width: 1px" name="C_firstname{{i}}"></ion-input>\n              </ion-item>\n            </ion-col>\n          <br>\n            <ion-col col-lg-auto>\n              <ion-item no-lines>\n                <ion-label class="fixedLabel">Middle Name:</ion-label>\n                <ion-input type="text" [(ngModel)]="childrenArray[i].middle_name" style="border: solid;border-width: 1px;" name="C_middlename{{i}}" ></ion-input>\n              </ion-item>\n            </ion-col>\n          <br>\n            <ion-col col-auto>\n              <ion-item no-lines>\n                <ion-label class="fixedLabel">Last Name: </ion-label>\n                <ion-input type="text" [(ngModel)]="childrenArray[i].last_name" style="border: solid;border-width: 1px" name = "C_lastname{{i}}"></ion-input>\n              </ion-item>\n            </ion-col>\n        <br>\n            <ion-col col-md-auto>\n              <ion-item no-lines>\n                <ion-label>DOB: </ion-label>\n                <button ion-button clear color="dark" type="button" item-right>\n                  <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="childrenArray[i].birthday" name="c_birthday{{i}}"></ion-datetime>\n                  <ion-icon name="arrow-dropdown" style="zoom:2.0;"> </ion-icon>\n                </button>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n        <ion-item no-lines>\n          <ion-grid item-content>\n            <ion-row>\n              <ion-col *ngFor="let item of gender; let t=index">\n                <ion-label>{{item}}</ion-label>\n                <ion-checkbox item-right [ngModel] = "section_gender[i] == t" (ngModelChange)="$event ? section_gender[i] = t : section_gender[i] = -1" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-item>\n        <br>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-auto>\n              <ion-item no-lines text-wrap>\n                <ion-label class="fixedLabel">Cultural Identity</ion-label>\n                <ion-input type="text" [(ngModel)]="childrenArray[i].c_id" style="border: solid;border-width: 1px" name="children_id{{i}}" ></ion-input>\n              </ion-item>\n            </ion-col>\n          <br>\n            <ion-col col-lg-auto offset-lg-2>\n              <ion-item no-lines text-wrap>\n                <ion-label class="fixedLabel">Currently Residing with:</ion-label>\n                <ion-input type="text" [(ngModel)]="childrenArray[i].currently_residing" style="border: solid;border-width: 1px" name="children_cr{{i}}" ></ion-input>\n              </ion-item>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item-group>\n    </ion-list>\n\n    <ion-item-divider><br><h2 text-wrap> Demographic Information </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-md-auto>\n          <ion-item no-lines>\n            <ion-label>Date of birth </ion-label>\n            <button ion-button clear color="dark" type="button" item-right>\n              <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="d_info.birthday" name="birthday"  (ngModelChange)="calculate_age()"></ion-datetime>\n              <ion-icon name="arrow-dropdown" style="zoom:2.0;"> </ion-icon>\n            </button>\n          </ion-item>\n        </ion-col>\n      <ion-col col-md-auto offset-md-2>\n        <ion-item no-lines>\n          <ion-label>Age: </ion-label>\n          <ion-input text-end [ngModel]="d_info.age" readonly="true" name="age">{{d_info.age}}</ion-input>\n        </ion-item>\n      </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-item no-lines>\n      <ion-grid item-content>\n        <ion-row>\n          <ion-col *ngFor="let item of single_married; let i=index">\n            <ion-label >{{item}}</ion-label>\n            <ion-checkbox item-right [ngModel]="section_sm == i"(ngModelChange)="$event ? section_sm = i : section_sm = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n\n      <ion-item no-lines>\n        <ion-grid item-content>\n          <ion-row><header>Aboriginal and Terrestraint islander people :</header></ion-row>\n          <ion-row>\n            <ion-col *ngFor="let item of section_2; let i=index">\n              <ion-label>{{item}}</ion-label>\n              <ion-checkbox item-right [ngModel]="section_atsi == i"(ngModelChange)="$event ? section_atsi = i : section_atsi = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n    <ion-item-group *ngIf="section_atsi == 0">\n      <ion-item no-lines text-wrap>\n        <ion-label class="fixedLabel">Details Identity</ion-label>\n        <ion-input type="text" [(ngModel)]="d_info.atsi_id" style="border: solid;border-width: 1px" name="atsi_id" ></ion-input>\n      </ion-item>\n      <br>\n      <ion-item no-lines text-wrap>\n        <ion-label class="fixedLabel">Cultural Identity</ion-label>\n        <ion-input type="text" [(ngModel)]="d_info.cultural_id" style="border: solid;border-width: 1px" name="c_id" ></ion-input>\n      </ion-item>\n      <br>\n      <ion-item no-lines text-wrap>\n        <ion-label>Country of Birth</ion-label>\n        <ion-select type="text" [(ngModel)]="d_info.country_birth" style="border: solid;border-width: 1px" name="c_birth" >\n          <ion-option *ngFor="let c of country_list" [value]="c">{{c}}</ion-option>\n        </ion-select>\n      </ion-item>\n      <br>\n      <ion-item no-lines text-wrap>\n        <ion-label>Date of Arrival in Australia</ion-label>\n        <button ion-button clear color="dark" type="button" item-right>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="d_info.arrival_date" name="arrive_d" ></ion-datetime>\n          <ion-icon name="arrow-dropdown" style="zoom:2.0;"> </ion-icon>\n        </button>\n      </ion-item>\n      <br>\n      <ion-item no-lines text-wrap>\n        <ion-label class="fixedLabel">Language Spoken at Home <span style="color: red">*</span></ion-label>\n        <ion-input type="text" [(ngModel)]="d_info.home_language" style="border: solid;border-width: 1px" name="h_lang" required #lang="ngModel"></ion-input>\n      </ion-item>\n      <!--<span *ngIf="lang.errors && lang.touched" text-end>-->\n                <!--<span [hidden]="!lang.errors.required">-->\n                    <!--<ion-label style="color: red; margin-left: 16px;"> This field is required </ion-label>-->\n                <!--</span>-->\n      <!--</span>-->\n      <br>\n\n      <ion-item>\n        <ion-grid item-content>\n          <ion-row><header>Interpreter: <span style="color: red">*</span></header></ion-row>\n          <ion-row>\n            <ion-col *ngFor="let item of section_2; let i=index">\n                <ion-label>{{item}} </ion-label>\n                <ion-checkbox item-right [ngModel]="section_Interpreter == i"(ngModelChange)="$event ? section_Interpreter = i : section_Interpreter = -1" [ngModelOptions]="{standalone: true}" required #name="ngModel"></ion-checkbox>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n    </ion-item-group>\n  </ion-list>\n\n    <ion-item-divider><br><h2 text-wrap> Services Required </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-item-group>\n      <ion-item>\n        <ion-label> Select the services </ion-label>\n        <ion-select multiple="True" [(ngModel)]="service_re.services_required" name="re_service" >\n          <ion-option *ngFor="let s of service_name" [value]="s">{{s}}</ion-option>\n        </ion-select>\n      </ion-item>\n      <br>\n      <ion-item no-lines>\n        <ion-label> Others : </ion-label>\n        <ion-textarea style="height: 100px;border: solid;border-width: 1px" [(ngModel)]="service_re.Other" name="other"></ion-textarea>\n      </ion-item>\n\n    </ion-item-group>\n  </ion-list>\n    <ion-item-divider><br> <h2>Other Information</h2></ion-item-divider>\n  <ion-list>\n    <ion-item-group>\n    <ion-item no-lines>\n      <ion-grid item-content>\n        <ion-row><header>Unmet Demand: </header></ion-row>\n        <ion-row>\n          <ion-col *ngFor="let item of section_2; let i=index">\n            <ion-label>{{item}} </ion-label>\n            <ion-checkbox item-right [ngModel]="section_Unmet == i"(ngModelChange)="$event ? section_Unmet = i : section_Unmet = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item no-lines>\n      <ion-grid item-content>\n        <ion-row><header>NAHA: </header></ion-row>\n        <ion-row>\n          <ion-col *ngFor="let item of section_2; let i=index">\n            <ion-label>{{item}} </ion-label>\n            <ion-checkbox item-right [ngModel]="section_Naha == i"(ngModelChange)="$event ? section_Naha = i : section_Naha = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n\n    <ion-item no-lines>\n      <ion-grid item-content>\n        <ion-row><header>Meet Criteri: </header></ion-row>\n        <ion-row>\n          <ion-col *ngFor="let item of section_2; let i=index">\n            <ion-label>{{item}} </ion-label>\n            <ion-checkbox item-right [ngModel]="section_MeetCriteri == i"(ngModelChange)="$event ? section_MeetCriteri = i : section_MeetCriteri = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    </ion-item-group>\n  </ion-list>\n\n    <ion-item-divider><br><h2 text-wrap> Pregnancy Status </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-item-group>\n      <ion-grid>\n      <ion-row>\n        <ion-col col-auto>\n          <ion-item no-lines>\n            <ion-label class="fixedLabel">Weeks: </ion-label>\n            <ion-input type="number" style="border: solid;border-width: 1px" name="week"></ion-input>\n          </ion-item>\n        </ion-col>\n      <br>\n        <ion-col col-md-auto offset-lg-2>\n          <ion-item no-lines>\n            <ion-label>EDB:</ion-label>\n            <button ion-button clear color="dark" type="button" item-right>\n              <ion-datetime displayFormat="DD/MM/YYYY" min="{{today}}" max="2030" [(ngModel)]="pre_status.edb" name="edb" (ngModelChange)="days_left()"></ion-datetime>\n              <ion-icon name="arrow-dropdown" style="zoom:2.0;"> </ion-icon>\n            </button>\n          </ion-item>\n        </ion-col>\n        <ion-col col-lg-auto>\n          <ion-item>\n            <ion-label>Days left</ion-label>\n            <ion-input text-end [ngModel]="edb_left" readonly="true" name="edb_left">{{edb_left}}</ion-input>>\n          </ion-item>\n        </ion-col>\n      <br>\n      </ion-row>\n      </ion-grid>\n    </ion-item-group>\n  </ion-list>\n  <ion-item-divider><br> <h2 text-wrap> Presenting Situation </h2></ion-item-divider>\n  <ion-list>\n    <br>\n      <ion-item no-lines>\n        <ion-label> Summary : </ion-label>\n        <ion-textarea style="height: 100px;border: solid;border-width: 1px" [(ngModel)]="presenting_summary.summary" name="summary"></ion-textarea>\n      </ion-item>\n  </ion-list>\n\n  <ion-item-divider><br><h2 text-wrap> Follow up required? </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-item no-lines>\n      <ion-grid item-content>\n        <ion-row>\n          <ion-col *ngFor="let item of section_2; let i=index">\n            <ion-label>{{item}} </ion-label>\n            <ion-checkbox item-right [ngModel]="follow_up_required == i"(ngModelChange)="$event ? follow_up_required = i : follow_up_required = -1" [ngModelOptions]="{standalone: true}" ></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item-group *ngIf="follow_up_required == 0">\n      <ion-item no-lines>\n        <ion-label> Detial : </ion-label>\n        <ion-textarea  style="height: 100px;border: solid;border-width: 1px" [(ngModel)]="follow_up.details" name="f_detial" ></ion-textarea>\n      </ion-item>\n    </ion-item-group>\n  </ion-list>\n\n  <ion-item-divider><br><h2 text-wrap> Administrative Information </h2></ion-item-divider>\n\n  <ion-list>\n    <br>\n    <ion-item-group>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-lg-auto>\n            <ion-item no-lines>\n              <ion-label>Date and Time: </ion-label>\n              <button ion-button clear color="dark" type="button" item-right>\n                <ion-datetime displayFormat="DD/MM/YYYY hh:mm:A" [(ngModel)]="admin_info.date" name="admin_date" ></ion-datetime>\n                <ion-icon name="arrow-dropdown" style="zoom:2.0;"> </ion-icon>\n              </button>\n            </ion-item>\n          </ion-col>\n        <br>\n          <ion-col col-lg-auto offset-lg-3>\n            <ion-item no-lines>\n              <ion-label class="fixedLabel" text-wrap>Call Length (Minutes):</ion-label>\n              <ion-input type="number" style="border: solid;border-width: 1px;" [(ngModel)]="admin_info.call_len" name="call_len" ></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <br>\n      <ion-item no-lines>\n        <ion-grid item-content>\n          <ion-row><header>Referred to other agencies: </header></ion-row>\n          <ion-row>\n            <ion-col *ngFor="let item of section_2; let i=index">\n              <ion-label>{{item}} </ion-label>\n              <ion-checkbox item-right [ngModel]="referred_link == i"(ngModelChange)="$event ? referred_link = i : referred_link = -1" [ngModelOptions]="{standalone: true}"></ion-checkbox>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n      <br>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-lg-auto>\n            <ion-item no-lines>\n              <ion-label class="fixedLabel" text-wrap>Completed By:</ion-label>\n              <ion-input type="text" style="border: solid;border-width: 1px;" [(ngModel)]="admin_info.completed_by" name="completed_by" ></ion-input>\n            </ion-item>\n          </ion-col>\n      <br>\n          <ion-col col-lg-auto offset-lg-2>\n            <ion-item no-lines text-wrap>\n              <ion-label class="fixedLabel" text-wrap>Referred From (Agency/Worker):</ion-label>\n              <ion-input type="text" style="border: solid;border-width: 1px;" [(ngModel)]="admin_info.referred_form" name="referred_form" ></ion-input>\n            </ion-item>\n          </ion-col>\n      <br>\n          <ion-col col-auto>\n            <ion-item no-lines>\n              <ion-label class="fixedLabel" text-wrap>Contact Information:</ion-label>\n              <ion-input type="text" style="border: solid;border-width: 1px;" [(ngModel)]="admin_info.contact_info" name="contact" ></ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item-group>\n  </ion-list>\n\n  <button ion-button block outline color="blue" [disabled]="!myForm.form.valid" type="submit"><b>Submit</b></button>\n    <span *ngIf="!myForm.form.valid" text-center>\n                <span [hidden]="myForm.form.valid">\n                    <ion-label style="color: red; margin-left: 16px;"> Please complete all necessary fields </ion-label>\n                </span>\n      </span>\n  </form>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/shi/WebstormProject/test/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = 'http://127.0.0.1:5000';
    }
    RestProvider.prototype.postForm = function (path, data) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/json');
        var body = JSON.stringify(data);
        // console.log(JSON.stringify(form.value));
        var res = this.http.post(this.apiUrl + path, body, { headers: headers })
            .subscribe(function (data) {
            console.log(data);
            if (data == 'success') {
                alert('Success');
            }
            else {
                alert('Fail to submit');
            }
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(221);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_rest_rest__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common_http__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_9__providers_rest_rest__["a" /* RestProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/shi/WebstormProject/test/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/shi/WebstormProject/test/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Country; });
var Country = {
    country_list: ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas",
        "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands",
        "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica",
        "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea",
        "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana",
        "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong (China)", "Hungary", "Iceland", "India",
        "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia",
        "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania",
        "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia",
        "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal",
        "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles",
        "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan",
        "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan (China)", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia",
        "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay",
        "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"]
};
//# sourceMappingURL=country.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map