import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  splash = true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  async doLogin(user) {
    try {
      this.navCtrl.setRoot(HomePage)
    } catch (e) {
      console.log(e);
    }
  }

  async passwordReset(user){
    try{
      console.log('password reset')
    }catch(e){
      console.error(e);
    }
  }



  register() {
    console.log('register')
  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
  }

}
