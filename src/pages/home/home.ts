import { Component } from '@angular/core';
import {DateTime, NavController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormArray} from "@angular/forms";
import {Refered_before} from "../../model/Referral_Form/referred_before";
import {Client_name} from "../../model/Referral_Form/client_name";
import {Address} from "../../model/Referral_Form/address";
import {Demographic_info} from "../../model/Referral_Form/demographic_info";
import {Service_required} from "../../model/Referral_Form/service_required";
import {Other_info} from "../../model/Referral_Form/other_info";
import {Follow_up} from "../../model/Referral_Form/follow_up";
import {Admin_info} from "../../model/Referral_Form/admin_info";
import {Pregnancy_statu} from "../../model/Referral_Form/pregnancy_statu";
import {Presenting_situation} from "../../model/Referral_Form/presenting_situation";
import {RestProvider} from "../../providers/rest/rest";
import {Country} from "../../model/constant_list/country";
import {Children_info} from "../../model/Referral_Form/children_info";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //data model
  refered_before = {} as Refered_before;
  client_name = {} as Client_name;
  client_address = {} as Address;
  d_info = {} as Demographic_info;
  service_re = {} as Service_required;
  other_info= {} as Other_info;
  follow_up = {} as Follow_up;
  admin_info = {} as Admin_info;
  pre_status ={} as Pregnancy_statu;
  presenting_summary = {} as Presenting_situation;
  children_info ={} as Children_info;
  childrenArray:any=[];
  children_number: any=[];
  number : number=0;

  fg: FormGroup;
  checkedIdx = 1;
  section_2_index = 1;
  section_atsi = 1;
  section_Interpreter = 1;
  section_Unmet = 1;
  section_Naha = 1;
  section_MeetCriteri = 1;
  section_sm = 1;
  follow_up_required = 1;
  referred_link = 1;
  section_gender: any=[];


  edb_left: any;
  today:any;

  service_name = [
    'Housing at Karyina',
    'Outreach',
    'Telephone Counselling',
    'Material Assistance',
    'Information and Retrieval',
    'Other'
  ];

  options = [
    'Yes',
    'No',
    'Unknown'
  ];

  section_2 = [
    'Yes',
    'No'
  ];
  single_married =[
    'Single',
    'Married'
  ];

  gender =[
    'Male',
    'Female'
  ];

  country_list = Country.country_list;

  constructor(public navCtrl: NavController, private formBulider: FormBuilder,public rest: RestProvider) {

    let today =new Date();
    let dd = today.getMonth()+1;
    let day;
    if (dd <= 9){
      day = '0'+dd;
    }
    this.today = today.getFullYear()+'-'+day+'-'+today.getDate();
    console.log(this.today);

    // this.myGroup = formBulider.group({
    //   type_housing:['',Validators.required],
    //   // birthday:['',Validators.required],
    //   // age:['',Validators.required]
    //   // atsi_id:['',Validators.required]
    //
    //
    // });
    //
    // this.client_address.type_housing = this.myGroup.controls['type_housing'];
    // // this.d_info.birthday = this.myGroup.controls['birthday'];
    // // this.d_info.age = this.myGroup.controls['age'];
    // // this.section_atsi = this.myGroup.controls['atsi_id']
    this.d_info.age = 0;

  }



  addChildren(){
    this.section_gender.push(1);
    this.childrenArray.push({});
    console.log(this.childrenArray);
    console.log(this.section_gender)
  }

  removeChildren(){
    this.section_gender.pop();
    this.childrenArray.pop();
  }

  days_left(){
    let oneDay = 24*60*60*1000

    let dateTime = new Date();
    let edb =new Date(this.pre_status.edb);
    let diffDays = Math.round(Math.abs((edb.getTime() - dateTime.getTime())/(oneDay)));
    this.edb_left = diffDays;
  }
  convent(choose:string){
    if (choose == "Yes"){
      return true;
    }else{
      return false;
    }
  }

  calculate_age(){
    let dateTime = new Date();
    let age = dateTime.getFullYear();
    this.d_info.age = age - parseInt(this.d_info.birthday);

  }

  sendForm(form){

    this.client_name.name_known = this.convent(this.section_2[this.section_2_index]);
    this.refered_before.referred = this.options[this.checkedIdx];
    this.d_info.atsi = this.convent(this.section_2[this.section_atsi]);
    this.d_info.interpreter = this.convent(this.section_2[this.section_Interpreter]);
    this.d_info.single_married = this.single_married[this.section_sm];

    for(var i =0;i<this.childrenArray.length;i++){
      this.childrenArray[i].Gender = this.gender[this.section_gender[i]];
    }
    console.log(this.childrenArray)

    let data = {
      "referred_before": this.refered_before,
      "client_name": this.client_name,
      "address": this.client_address,
      "demographic Information": this.d_info,
      "service_required": this.service_re,
      "other_info": this.other_info,
      "pregnancy_status": this.pre_status,
      "presenting_situation": this.presenting_summary,
      "follow_up": this.follow_up_required,
      "admin_info": this.admin_info,
      "children_info": this.childrenArray
    };
    console.log(JSON.stringify(data));
    let res = this.rest.postForm('/json-example',data);

  }

  ionViewDidLoad() {

  }


}
